# Exercice 6 TP1

## Objectif
L'objectif de cet exercice est d'améliorer la solution développée dans l'exercice précédent, et d'effectuer les Tests sous la forme d'une classe avec le module unittest.

## Organisation du projet
Nous avons globalement la même organisation que l'exercice précédent : 

    .
    ├── images
    │   └── pytest.png
    ├── Package_Calculator
    │   ├── Calculator.py
    │   ├── __init__.py
    │   └── __pycache__
    │       └── Calculator.cpython-35.pyc
    ├── Package_Test
    │   ├── Calculator_test.py
    │   ├── __init__.py
    │   └── __pycache__
    │       └── Calculator_test.cpython-35-pytest-5.4.1.pyc
    └── README.md


## Réalisation
Pour créer la classe de Test, on a créé quatres classes qui gèrent chacune une opération. Dans chaque classe nous avons créé des méthodes permettant de gérer chaque possibilité de sortie de chaque fonction de la classe SimpleCalculator.
Pour vérifier que les résultats sont cohérents avec ce qu'on attends, on utilise le assertEqual(resulat, "resultat attendu"), qui indique ensuite si le test a été réussit ou si le test a échoué.


## Execution du script
Dans un premier temps il faut régler le PYTHON PATH pour le placer à la racine du repo, commande :
 
    export PYTHONPATH=$PYTHONPATH:'.'
    
Puis on peut utiliser pytest pour effectuer directement les test, pour cela il faut installer pytest, (installation avec pip3 car sur la VM xubuntu) :

    pip3 install pytest
    
Enfin en se plaçant à la racine du repo on peut effectuer la commande :

    pytest
    

## Résultats 
Lorsqu'on effectue l'éxecution de pytest, on obtient alors le résultat :

![result](images/pytest.png)
 
 
On voit donc que nous avons nos 13 tests qui ont bien été effectués.
